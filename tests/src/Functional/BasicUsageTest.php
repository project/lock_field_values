<?php

namespace Drupal\Tests\lock_field_values\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Test description.
 *
 * @group lock_field_values
 */
class BasicUsageTest extends BrowserTestBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['lock_field_values', 'node', 'field', 'field_ui'];

  /**
   * A node to use for testing.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * A test admin user.
   *
   * @var \Drupal\User\UserInterface
   */
  protected $admin_user;

  /**
   * A test editor.
   *
   * @var \Drupal\User\UserInterface
   */
  protected $editor_user;

  /**
   * A test editor.
   *
   * @var \Drupal\User\UserInterface
   */
  protected $editor_user2;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Create user 1.
    $this->createUser();

    $this->drupalCreateContentType(['type' => 'page']);

    // Create a new text field to test with.
    FieldStorageConfig::create([
      'field_name' => 'field_locky',
      'entity_type' => 'node',
      'type' => 'text',
    ])->save();
    FieldConfig::create([
      'field_name' => 'field_locky',
      'label' => 'Locky McLockerson',
      'entity_type' => 'node',
      'bundle' => 'page',
      'third_party_settings' => [
        'lock_field_values' => [
          'lockable' => TRUE,
        ],
      ],
    ])->save();
    $display_repo = \Drupal::service('entity_display.repository');
    $display_repo->getFormDisplay('node', 'page', 'default')
      ->setComponent('field_locky', [
        'type' => 'text_textfield',
      ])
      ->save();
    $display_repo->getViewDisplay('node', 'page', 'default')
      ->setComponent('field_locky', [
        'type' => 'text_default',
        'weight' => 1,
      ])
      ->save();

    $this->node = $this->drupalCreateNode([
      'type' => 'page',
    ]);

    $this->admin_user = $this->drupalCreateUser([
      'administer content types',
      'administer node fields',
    ]);
    $this->editor_user = $this->drupalCreateUser([
      'lock and unlock fields',
      'edit any page content',
      'create page content',
    ]);
    $this->editor_user2 = $this->drupalCreateUser([
      'edit any page content',
    ]);
  }

  /**
   * Test admin user's ability to allow a field to be unlocked and locked.
   */
  public function testLockable() {
    // Check that admin users can enable specific fields to be unlockable.
    $this->drupalLogin($this->admin_user);
    $this->drupalGet('admin/structure/types/manage/page/fields/node.page.field_locky');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementExists('xpath', '//label[text() = "Administrator can lock values"]');
    $edit = [
      'third_party_settings[lock_field_values][lockable]' => FALSE,
    ];
    $this->drupalPostForm(NULL, $edit, $this->t('Save settings'));
    $this->drupalGet('admin/structure/types/manage/page/fields/node.page.field_locky');
    $this->assertNoFieldChecked('edit-third-party-settings-lock-field-values-lockable');

    // Check that admin users can make a field lockable.
    $edit = [
      'third_party_settings[lock_field_values][lockable]' => TRUE,
    ];
    $this->drupalPostForm(NULL, $edit, $this->t('Save settings'));
    $this->drupalGet('admin/structure/types/manage/page/fields/node.page.field_locky');
    $this->assertFieldChecked('edit-third-party-settings-lock-field-values-lockable');
  }

  /**
   * Test editor's ability to lock a field.
   */
  public function testLockField() {
    // Check that privileged editors can lock a field value.
    $this->drupalLogin($this->editor_user);
    $this->drupalGet('node/' . $this->node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//label[text() = "Lock field values"]');
    $edit = [
      'lock_field_values[field_locky]' => TRUE,
    ];
    $this->drupalPostForm(NULL, $edit, $this->t('Save'));
    $this->drupalGet('node/' . $this->node->id() . '/edit');
    $this->assertFieldChecked('edit-lock-field-values-field-locky');

    // Check that non-privileged editors cannot lock a field value.
    $this->drupalLogin($this->editor_user2);
    $this->drupalGet('node/' . $this->node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('xpath', '//label[text() = "Lock field values"]');

    // Check that non-privileged editors cannot edit a locked field value.
    $this->drupalGet('node/' . $this->node->id() . '/edit');
    $edit = [
      'field_locky[0][value]' => 'Bruce Wayne',
    ];
    $this->drupalPostForm(NULL, $edit, $this->t('Save'));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->pageTextNotContains('Bruce Wayne');

    // Check that privileged editors can unlock a field value.
    $this->drupalLogin($this->editor_user);
    $this->drupalGet('node/' . $this->node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//label[text() = "Lock field values"]');
    $edit = [
      'lock_field_values[field_locky]' => FALSE,
    ];
    $this->drupalPostForm(NULL, $edit, $this->t('Save'));
    $this->drupalGet('node/' . $this->node->id() . '/edit');
    $this->assertNoFieldChecked('edit-lock-field-values-field-locky');

    // Check that non-privileged editors can edit an unlocked field value.
    $this->drupalGet('node/' . $this->node->id() . '/edit');
    $edit = [
      'field_locky[0][value]' => 'Bruce Wayne',
    ];
    $this->drupalPostForm(NULL, $edit, $this->t('Save'));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->pageTextContains('Bruce Wayne');
  }

}
